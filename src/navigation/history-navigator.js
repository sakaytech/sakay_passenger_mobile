/**
 * @format
 * @flow
 */

import React from 'react';
import { createStackNavigator } from 'react-navigation';
import HistoryList from '../screen/history';

type Props = {
  navigation: any
}

const routeConfigs = {
  History: {
    screen: HistoryList
  }
};
const stackNavigatorConfig = {
  initialRouteName: 'History'
};

/**
 *
 * @reference https://reactnavigation.org/docs/en/stack-navigator.html#docsNav
 * @type {NavigationContainer}
 */
const HistoryNavigator = createStackNavigator(routeConfigs, stackNavigatorConfig);

class HistoryScreen extends React.Component<Props> {
  static router = HistoryNavigator.router;
  
  render () {
    return <HistoryNavigator navigation={this.props.navigation}/>;
  }
}

export default HistoryScreen;
