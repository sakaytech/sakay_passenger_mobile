/**
 * @format
 * @flow
 */

import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import AuthenticationScreen from './authentication-navigator';
import BookingScreen from './booking-navigator';
import HistoryScreen from './history-navigator';

const routeConfigs = {
  Authentication: AuthenticationScreen,
  Booking: BookingScreen,
  History: HistoryScreen
};

const stackNavigatorConfig = {
  initialRouteName: 'Authentication'
};

/**
 * Set screens for navigation
 * @documentation https://reactnavigation.org/docs/en/switch-navigator.html#docsNav
 * @type {NavigationContainer}
 */
const AppNavigator = createSwitchNavigator(routeConfigs, stackNavigatorConfig);

export default createAppContainer(AppNavigator);




